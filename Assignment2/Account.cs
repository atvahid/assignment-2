﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2
{
    /// <summary>
    ///  Defines the valid account types that can be created by the user. Note that the 
    /// application creates default general accounts as well but the user can only create
    /// accounts designated through the enum values below.
    /// </summary>
    enum AccType
    {
        accountTypeChequing = 1,
        accountTypeSavings
    }

    /// <summary>
    /// Represents the account
    /// </summary>
    class Account
    {
        /// <summary>
        /// The name of the account owner
        /// </summary>
        private string _acctHolderName;

        /// <summary>
        /// The account number
        /// </summary>
        private int _acctNo;

        /// <summary>
        /// the annual interest rate applicable on the balance.Note the visibility
        /// of the field variable is set to "protected" to allow it to be accessed by derived classes.
        /// </summary>
        protected float _annualIntrRate;

        /// <summary>
        /// the account balance that gets affected by withdrawls and deposits. Note the visibility
        /// of the field variable is set to "protected" to allow it to be accessed by derived classes.
        /// NOTE: the type is used as a double while the annual interest rate is float just to showcase
        /// the two floating point types. In practice financial application actually would use "decimals"
        /// which provide most precision.
        /// </summary>
        protected double _balance;

        /// <summary>
        /// Initialize the account object with its attributes.
        /// The account constructor requires the caller to supply an account number and
        /// the name of the account holder in order to create an account. 
        /// 
        /// NOTE: the constructor assigns default values to each parameter allowing the code
        /// not to supply them (i.e. acct = Account()). If the calling code does not supply
        /// values for the two parameters they will receive these default values. This is used
        /// when the accounts are created from data files 
        /// </summary>
        public Account(int acctNo = -1, string acctHolderName = "")
        {
            //Inititialize the _acctHolderName variable
            _acctHolderName = acctHolderName;

            //Inititialize the _acctNo variable
            _acctNo = acctNo;

            //Initialize the _balance variable
            _balance = 0.0f;

            //Initialize the _annualIntrRate variable
            _annualIntrRate = 0.0f;
        }

        /// <summary>
        /// Declare read only property for the name of the account holder
        /// </summary>
        public string AccountHolderName
        {
            get { return _acctHolderName; }
        }

        /// <summary>
        /// Declare read only property for the account number
        /// </summary>
        public int AccountNumber
        {
            get { return _acctNo; }
        }

        /// <summary>
        /// Declare the read and write properties of the annual interest rate
        /// Note: the user will enter a percentage, in order for the input to compute with program the input is
        /// divided by 100 to get annual interest rate as a decimal
        /// </summary>
        public virtual float AnnualIntrRate
        {
            get { return _annualIntrRate; }
            set { _annualIntrRate = value / 100f; }
        }

        /// <summary>
        /// Declare the read only property for Balance
        /// </summary>
        public double Balance
        {
            get { return _balance; }
        }

        /// <summary>
        /// TODO: Declare deposit()
        /// </summary>
        public virtual double Deposit(double amount)
        {
            //Deposits the amount given and returns the new balance
            if(amount < 0)
            {
                Console.WriteLine("Invalid amount provided. Cannot deposit a negative amount");
            }

            //change the balance
            _balance += amount;



            return _balance;
        }

        /// <summary>
        /// TODO: Declare withdraw()
        /// </summary>
        public virtual double withdraw(float amount)
        {
            //Withdraws the amount given from the account and returns the new balance
            if(amount < 0)
            {
                Console.WriteLine("Invalid amount provided. Cannot withdraw a negative amount");
                return 0;
            }

            if(amount > _balance)
            {
                Console.WriteLine("Insufficient funds. Cannot withdraw the provided amount");
                return 0;
            }

            //withdraw amount
            _balance -= amount;


            return _balance;
        }

        /// <summary>
        /// TODO: Declare load()
        /// </summary>
        public void load(StreamReader accFileReader)
        {
            //Loads the account information from the given file
            _acctNo = Convert.ToInt32(accFileReader.ReadLine());
            _acctHolderName = accFileReader.ReadLine();
            _balance = Convert.ToSingle(accFileReader.ReadLine());
            _annualIntrRate = Convert.ToSingle(accFileReader.ReadLine());
        }

        /// <summary>
        /// TODO: Declare save()
        /// </summary>
        public void save(StreamWriter acctFileWriter)
        {
            //Saves the account information the the file given 
            acctFileWriter.Write(_acctNo);
            acctFileWriter.Write(_acctHolderName);
            acctFileWriter.Write(_balance);
            acctFileWriter.Write(_annualIntrRate);
        }

    }
}
