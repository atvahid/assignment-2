﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2
{
    /// <summary>
    /// Represents the main menu options 
    /// </summary>
    enum MainMenuOpt
    {
        selectAccountOpt = 1,
        createAccountOpt,
        exitAtmOpt
    }

    /// <summary>
    /// Represents the account menu options 
    /// </summary>
    enum AccountMenuOpt
    {
        checkBalanceOpt = 1,
        withdrawOpt,
        depositOpt,
        displayTransactions,
        exitAccountOpt
    }

    /// <summary>
    /// Class Atm that represents the ATM machine which displays and performs the account functions on
    /// a given bank account (checking balance, withdrawing, depositing money  
    /// </summary>
    class Atm
    {
        /// <summary>
        /// Represents the Bank object that the Atm works with
        /// </summary>
        private Bank _bank;

        /// <summary>
        /// Represents the Transaction object class
        /// </summary>
        private Transaction _transaction;

        /// <summary>
        /// Constructor for Atm class
        /// </summary>
        public Atm(Bank bank)
        {
            //Initialize the _bank variable
            _bank = bank;
        }

        /// <summary>
        /// Starts the ATM program by displaying the user options
        /// </summary>
        public void start()
        {
            //Implement a while loop that displays the menu until the user chooses to exit
            while (true)
            {
                //display the main menu and perform the main actions depending on the user's choice
                var selectedOpt = ShowMainMenu();

                if (selectedOpt == MainMenuOpt.selectAccountOpt)
                {
                    //the user enters their account ID 
                    var selectAcct = onSelectAccount();

                    if (selectAcct != null)
                    {
                        //based on account ID, corresponding account and its options display
                        onManageAccount(selectAcct);
                    }
                }
                else if (selectedOpt == MainMenuOpt.createAccountOpt)
                {
                    //Create account for user 
                    onCreateAccount();
                }
                else if (selectedOpt == MainMenuOpt.exitAtmOpt)
                {
                    //If user chooses to exit the atm application then save all banking infromation and exit
                    _bank.saveAccountData();
                }
                else
                {
                    //if user entered invalid input, then let the user know 
                    Console.WriteLine("Please enter valid menu option");
                }
                //handle the user selection by calling the appropriate event handler method
            }
        }

        /// <summary>
        /// Displays the ATM menu 
        /// </summary>
        public MainMenuOpt ShowMainMenu()
        {
            //Display menu and obtain selection from user
            Console.WriteLine("\nMain Menu\n\n1: Select Account\n2: Create Account\n3: Exit\n\nEnter a choice: ");
            int userChoice = Convert.ToInt32(Console.ReadLine());

            //Convert user's input to type MainMenuOpt and return users choice
            MainMenuOpt choice = (MainMenuOpt)userChoice;
            return choice;
        }

        /// <summary>
        /// Displays the Account menu
        /// </summary>
        public AccountMenuOpt showAccountMenu()
        {
            //Display menu and obtain selection from user. Return selected option.
            Console.WriteLine("\nAccount menu\n\n1. Check Balance\n2. Withdraw\n3. Deposit\n4. Display Transactions\n5. Exit Account");
            int userChoiceAcct = Convert.ToInt32(Console.ReadLine());

            //Convert user's input to type AccountMenuOpt and return users choice
            AccountMenuOpt choiceAcct = (AccountMenuOpt)userChoiceAcct;
            return choiceAcct;
        }

        /// <summary>
        /// Create and open account (the user is prompted for all account information 
        /// </summary>
        public void onCreateAccount()
        {
            //Implement while loop to ensure user selects an option
            while (true)
            {
                //Get the name of the account holder from user by calling promptForClientName()
                string clientName = promptForClientName();

                //Get initial deposit amount from user by calling promptForDepositAmount()
                double initDepAmount = promptForDepositAmount();

                //Get annual interest rate from user by calling promptForAnnualIntrRate()
                float annualIntrRate = promptForAnnualIntrRate();

                //Get the account type from user by calling promptForAccountType()
                var acctType = promptForAccountType();

                //open the account
                Account newAccount = _bank.openAccount(clientName, acctType);

                //Set other properties of the account by calling the deposit and setAnnualIntrRate from Account class
                newAccount.Deposit(initDepAmount);
                newAccount.AnnualIntrRate = annualIntrRate;

                //the account has been successfully created and added to the bank
                return;
            }
        }

        /// <summary>
        /// Selects an account (prompts user for account number and remembers which account was selected) 
        /// </summary>
        public Account onSelectAccount()
        {
            //Attempt the user interaction  until all user information is provided correctly or the user cancels
            while (true)
            {
                //Prompt the user for the account number to select
                Console.WriteLine("Please enter your account ID or press ENTER to cancel: ");
                var acctNum = Console.ReadLine();

                //check to see if the user gave up and is cancelling the operation      
                if (acctNum.Length == 0)
                {
                    return null;
                }

                //the user entered an account number get the actual number
                int realAcct = Convert.ToInt32(acctNum);

                //obtain the account required by the user from the bank
                var account = _bank.findAccount(realAcct);

                if(account != null)
                {
                    //if account is not empty, return account
                    return account;
                } 
                else
                {
                    //prompt user that the account couldn't be found 
                    Console.WriteLine("The account was not found, Please select a different account");
                }
            }
        }

        /// <summary>
        /// Manage the account by allowing user to execute operation on given account
        /// </summary>
        public void onManageAccount(Account account)
        {
            //Implement while loop to ensure user selects valid option
            while (true)
            {
                //display menu and obtain a user selection
                var selectAcctOpt = showAccountMenu(); 

                if (selectAcctOpt == AccountMenuOpt.checkBalanceOpt)
                {
                    //call event handler method 'onCheckBalance',  if user selects to check the balance
                    onCheckBalance(account);
                }
                else if (selectAcctOpt == AccountMenuOpt.depositOpt)
                {
                    //call event handler method 'onCheckBalance',  if user selects to make a deposit
                    onDeposit(account);
                }
                else if (selectAcctOpt == AccountMenuOpt.withdrawOpt)
                {
                    //call event handler method 'onCheckBalance',  if user selects matching account menu option
                    onWithdraw(account);
                }
                else if (selectAcctOpt == AccountMenuOpt.displayTransactions)
                {
                    //load file from transaction class
                    _transaction.LoadTransactions();
                }
                else if (selectAcctOpt == AccountMenuOpt.exitAccountOpt)
                {
                    //return to main menu if user selects to exit
                    return;
                }
                else
                {
                    Console.WriteLine("Please enter a valid account menu option");
                }

                //handle the user selection with the appropriate event handler
            }
        }

        /// <summary>
        /// Prints the balance in the given account
        /// </summary>
        public void onCheckBalance(Account account)
        {
            //display balance in the account given by calling getBalance() from the Account class
            Console.WriteLine($"The account balance is ${account.Balance}");
        }

        /// <summary>
        /// Prompts the user for an amount and performs the deposit. Handles any errors related to incorrect amounts
        /// </summary>
        public void onDeposit(Account account)
        {
            //repeat trying to ask the user for the required input until the input is correct or the user cancels
            while (true)
            {
                //obtain the input amount from the user
                Console.WriteLine("Please enter the amount you want to deposit or press ENTER to cancel: ");
                var amount = Console.ReadLine();

                //test for empty input in case the user pressed [ENTER] because they wanted to give up on depositing money
                if (amount.Length > 0)
                {
                    //convert amount to float and deposit money by calling the deposit method with the 
                    //account object parameter
                    float depositAmount = Convert.ToSingle(amount);
                    account.Deposit(depositAmount);

                    //save in transactions
                    int accNumber = account.AccountNumber;
                    string transaction = "Deposited";
                    _transaction.DetermineTransactions(accNumber, transaction, depositAmount);   
                }
                
                //the deposit was done or user entered nothing so break from the infinite loop
                return;
            }
        }

        /// <summary>
        ///Prompts the user for amount and performs the withdraw. Handles any errors related to incorrect amounts
        /// </summary>
        public void onWithdraw(Account account)
        {
            while (true)
            {
                //obtain the input amount from the user
                Console.WriteLine("Please enter the amount you want to withdraw or press ENTER to cancel: ");
                var inputAmount = Console.ReadLine();

                //test for empty input in case the user pressed [ENTER] because they wanted to give up on withdrawing money
                if (inputAmount.Length > 0)
                {
                    //convert amount to float and withdraw money by calling the withdraw method with the 
                    //account object parameter
                    float withdrawAmount = Convert.ToSingle(inputAmount);
                    account.withdraw(withdrawAmount);

                    //save in tranactions
                    int accNumber = account.AccountNumber;
                    string transaction = "Withdrew";
                    _transaction.DetermineTransactions(accNumber, transaction, withdrawAmount);
                }

                //the withdrawal was done or user entered nothing so break from the infinite loop
                return;
            }
        }

        /// <summary>
        /// Prompts the user to enter client name 
        /// </summary>
        public string promptForClientName()
        {
            //Prompt user to enter client name
            Console.WriteLine("Please enter the client name or press ENTER to cancel: ");
            string inputName = Console.ReadLine();

            //if statement to check if client name was entered
            if (inputName.Length == 0)
            {
                Console.WriteLine("The user has canceled this operation");
                return null;
            }
             
            //return client name
            return inputName;
        }

        /// <summary>
        /// Prompts the user to enter an account balance
        /// </summary>
        public double promptForDepositAmount()
        {
            //repeat trying to ask the user for the required input until the input is correct or the user cancels
            while (true)
            {
                //Prompt user to enter initial account balance and convert it to a double
                Console.WriteLine("Please enter the initial account balance: ");
                var inputInitAmount = Console.ReadLine();
                double initAmount = Convert.ToDouble(inputInitAmount);

                //if statement to chack if the user input is greater or equal to 0
                if (initAmount >= 0)
                {
                    //if user entered amount greater or equal to 0, return the amount user entered
                    return initAmount;
                }
                else
                {
                    //if amount is less than 0, display message to let them know that it is not a valid input
                    Console.WriteLine("Cannot create an account with a negative initial balance, Please enter a different amount");
                }
                 
            }
        }

        /// <summary>
        ///Prompts the user to enter the annual interest rate for an account
        /// </summary>
        public float promptForAnnualIntrRate()
        {
            //repeat trying to ask the user for the required input until the input is correct or the user cancels
            while (true)
            {
                //obtain the input from the user
                Console.WriteLine("Please enter the interest rate for this account");
                float inputIntrRate = Convert.ToSingle(Console.ReadLine()); 

                //Check if interest rate is greater than 0
                if (inputIntrRate >= 0)
                {
                    return inputIntrRate;
                }
                else
                {
                    //if the interest rate is less than 0 than prompt the user to let them know it is not a valid input
                    Console.WriteLine("Cannot create an account with a negative interest rate, Please enter a different interest rate");
                }

            }
        }

        /// <summary>
        /// Prompts the user to enter an account type
        /// </summary>
        public AccType promptForAccountType()
        {
            //repeat trying to ask the user for the required input until the input is correct or the user cancels
            while (true)
            {
                //Prompt user to enter account type
                Console.WriteLine("Please enter the account type (c/s: Chequing/Savings): ");
                string inputAcctType = Console.ReadLine();

                //if statement to check type of account entered
                if (inputAcctType == "c" || inputAcctType == "Chequing")
                {
                    //if user selected chequing account, return accountTypeChequing from AccType
                    return AccType.accountTypeChequing;
                }
                else if (inputAcctType == "s" || inputAcctType == "Savings")
                {
                    //if user selected savings account, return accountTypeSavings from AccType enumerater
                    return AccType.accountTypeSavings;
                }
                else
                {
                    //if user entered anything else, display message to enter valid option
                    Console.WriteLine("Invalid Input, Please enter one of the following supported anwsers (c/s: Chequing/Savings)");
                }
            }
        }
    }
}
