﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2
{
    /// <summary>
    /// Represents the savings account 
    /// </summary>
    class SavingsAccount : Account 
    {
        /// <summary>
        /// The matching deposit ratio. For every dollar deposit this account will
        /// automatically be credited with 0.5 dollars. Defined as a constant and accessible
        /// through the name of the class along with the DOT notation
        /// </summary>
        private const double MATCHING_DEPOSIT_RATIO = 0.5;

        /// <summary>
        /// The minimum interest rate for savings accounts. Defined as a constant and accessible
        /// through the name of the class along with the DOT notation 
        /// </summary>
        private const float MIN_INTEREST_RATE = 3.0f;

        /// <summary>
        /// Constructor for SavingsAccount class, If the calling code does not supply
        /// values for the two parameters they will receive these default values. This is used
        /// when the accounts are created from data files 
        /// </summary>
        public SavingsAccount(int acctNo, string acctHolderName) : base(acctNo, acctHolderName)
        {
            //As the checquing account doesn't have any specific field variables there is nothing
            //to initialize. However the constructor is required in order to pass data provided
            //by client code to the base class through the base(..) call
        }

        /// <summary>
        /// The Annual Interest Rate mutator is overridden in order to verify that the annual interest rate is valid 
        /// for a checquing account when setting the interest rate
        /// </summary>
        public override float AnnualIntrRate
        {
            set
            {
                //check to ensure the annual interest rate is valid for a savings account
                if (value < MIN_INTEREST_RATE)
                {
                    Console.WriteLine("A savings account cannot have an interest rate less than " + MIN_INTEREST_RATE);
                }
                else
                {
                    _annualIntrRate = value / 100f;
                }
            }
        }

        /// <summary>
        /// Deposit the given amount in the account and return the new balance. For every dollar deposited the
        /// account will be credited with 0.5 dollars with an automatic deposit, the new account balance AFTER 
        /// the amount was deposited to avoid a call to Balance.get if needed
        /// </summary>
        public override double Deposit(double amount)
        {
            //TODO: add auto deposit
            return base.Deposit(amount);
        }
    }
}
