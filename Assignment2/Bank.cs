﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2
{
    /// <summary>
    /// Represents a bank comprised of a list of accounts
    /// </summary>
    class Bank
    {
        /// <summary>
        /// A list of the accounts in the bank
        /// </summary>
        private List<Account> _accountList;

        /// <summary>
        /// The first account number
        /// </summary>
        private const int _defaultAccountNoStart = 100;

        /// <summary>
        /// The name of the folder containing all the account files
        /// </summary>
        private const string BANKING_DATA_FOLDER = "BankingData";

        /// <summary>
        /// Prefix used for account files storing regular account data
        /// </summary>
        private const string ACCT_FILE_PREFIX = "acct";

        /// <summary>
        /// Prefix used for account files storing checquing account data
        /// </summary>
        private const string CHQ_ACCT_FILE_PREFIX = "chqacct";

        /// <summary>
        /// Prefix used for account files storing savings account data
        /// </summary>
        private const string SAV_ACCT_FILE_PREFIX = "savacct";

        /// <summary>
        /// Constructor for the Bank class
        /// </summary>
        public Bank()
        {
            //Initialize the _accountList variable
            _accountList = new List<Account>();
        }

        /// <summary>
        /// Create and store an account object with the required attributes
        /// </summary>
        public Account openAccount(string clientName, AccType acctType)
        {
            //Prompt user for account number by calling determineAccountNumber()
            int accountNumber = determineAccountNumber();

            //create and store account with the required attributes
            if (acctType == AccType.accountTypeChequing)
            {
                //if chequing account was selected, create a new chequing account object
                ChequingAccount newAccount = new ChequingAccount(accountNumber, clientName);

                //add the new account to the list of accounts in the bank and return account to the caller so other properties can be set
                _accountList.Add(newAccount);
                return newAccount;
            }
            else if (acctType == AccType.accountTypeSavings)
            {
                //if savings account was selected, create a new savings account object
                SavingsAccount newAccount = new SavingsAccount(accountNumber, clientName);

                //add the new account to the list of accounts in the bank and return account to the caller so other properties can be set
                _accountList.Add(newAccount);
                return newAccount;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Load the account data for all accounts The account data files are stored in a directory
        /// named BankingData located in the current directory, the directory used to run the application from
        /// </summary>
        public void loadAccountData()
        {
            //get the directory path to the account files

            //get the list of files in the directory

            //go through the list of files, create the appropriate accounts and load the account files

            //if at this point the list of accounts is empty add the defaults accounts so the application is usable
        }

        /// <summary>
        /// Saves the data for all accounts in the data directory of the application. Each account is
        /// saved in a separate file which contains all the information and list of transactions performed
        /// in the account. The account data files are stored in a directory named BankingData located in the 
        /// current directory, the directory used to run the application from
        /// </summary>
        public void saveAccountData()
        {
            //locate the path to the account files

            //make the directory if it does not exist

            //go through each account in the list of accounts and ask it to save itself into a corresponding file
        }

        /// <summary>
        /// Create 10 accounts with predefined IDs and balances. The default accounts are created only
        /// if no account data files exist
        /// </summary>
        public void createDefaultAccounts()
        {
            //repeat for as many default accounts need to be created
            for (int iaccount = 1; iaccount <= 10; iaccount++)
            {
                Account newDefaultAcct = new Account(_defaultAccountNoStart + iaccount, "");

                //create the account with required default properties
                newDefaultAcct.Deposit(100);
                newDefaultAcct.AnnualIntrRate = 2.5f;

                //add the account to the list
                _accountList.Add(newDefaultAcct);
            }
        }

        /// <summary>
        /// Returns the account with the given account number or null if no account with that ID can be found
        /// </summary>
        public Account findAccount(int acctNo)
        {
            //go through all the accounts until one is found with the given account number
            foreach (Account account in _accountList)
            {
                if (account.AccountNumber == acctNo)
                {
                    return account;  
                }
            }

            //if the program got here it means there was no account with the given account number
            return null;
        }

        /// <summary>
        /// Determine the account number prompting the user until they enter the correct information.
        /// The method throws an OperationCancel exception if the user chooses to terminate.
        /// </summary>
        public int determineAccountNumber()
        {
            //repeat trying to ask the user for the required input until the input is correct or the user cancels
            while (true)
            {
                //ask the user for input
                Console.WriteLine("\nPlease enter the account number (100 - 1000) or press ENTER to cancel: ");
                var inputAccountNum = Console.ReadLine();
                
                //check whether the user cancelled the operation
                if (inputAccountNum.Length == 0)
                {
                    Console.WriteLine("User has chosen to cancel this operation");
                }

                //check the input to ensure correctness and deal with incorrect input
                int accountNum = Convert.ToInt32(inputAccountNum);

                if (accountNum < 100  || accountNum > 1000 )
                {
                    Console.WriteLine("\nThe account you entered is not valid, Please enter a different account number");
                }
                
                //check that the account number is not in use
                foreach(Account account in _accountList)
                {
                    if (accountNum == account.AccountNumber)
                    {
                        Console.WriteLine("\nThe account number you entered already exists. Please enter a different account number");
                    }
                }

                //the account number has been generated successfully
                return accountNum;
            }
        }
    }
}
