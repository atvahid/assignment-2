﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2
{
    /// <summary>
    /// Represents the transactions done in the atm
    /// </summary>
    class Transaction
    {
        /// <summary>
        /// Represents the account number 
        /// </summary>
        private int accountNumber;

        /// <summary>
        /// Represents the type of transaction either 'deposit' or 'withdraw'
        /// </summary>
        private string TypeTransaction;

        /// <summary>
        /// Represents the amount the transaction worked with
        /// </summary>
        private float accountAmount;

        /// <summary>
        /// Represents the transaction file 
        /// </summary>
        private StreamWriter _file;

        //Consturctor for Transction class
        public Transaction()
        {

        }

        public void DetermineTransactions(int acctNo, string typeOfTransaction, float amounts)
        {
            //Initialize accountNumber
            accountNumber = acctNo;

            //Initialize TypeTransaction
            TypeTransaction = typeOfTransaction;

            //Initialize accountAmount
            accountAmount = amounts;

            //call the record transactions class
            RecordTransactions(_file);
        }

        /// <summary>
        /// Records the transactions
        /// </summary>
        public void RecordTransactions(StreamWriter file)
        {
            //record the type of transaction done with the amount and the matching account number it was under
            file.Write($"For account {accountNumber}: \n{TypeTransaction} - ${accountAmount}");
        }

        /// <summary>
        /// Displays the transactions
        /// </summary>
        public void LoadTransactions()
        {
            //display all transaction done
            Console.Write(_file);
        }
    }
}
