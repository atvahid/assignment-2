﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2
{
    /// <summary>
    /// Represents the chequing account that has an overdraft limit and a maximum interest rate 
    /// </summary>
    class ChequingAccount : Account
    {
        /// <summary>
        /// The amount of overdraft is constant. Defined as such and accessible
        /// through the name of the class along with the DOT notation
        /// </summary>
        private const double OVERDRAFT_LIMIT = 500.0;

        /// <summary>
        /// The maximum interest rate for checquing accounts. Defined as such and accessible
        /// through the name of the class along with the DOT notation
        /// </summary>
        private const float MAX_INTEREST_RATE = 1.0f;

        /// <summary>
        /// Constructor for ChequingAccount class, assigns default values to each parameter allowing the code
        /// not to supply them (i.e. acct = ChequingAccount()). If the calling code does not supply
        /// values for the two parameters they will receive these default values. This is used
        /// when the accounts are created from data files
        /// </summary>
        public ChequingAccount(int acctNo = -1, string acctHolderName = "") : base(acctNo, acctHolderName)
        {
            //As the checquing account doesn't have any specific field variables there is nothing
            //to initialize. However the constructor is required in order to pass data provided
            //by client code to the base class through the base(..) call
        }

        /// <summary>
        /// Sets annual interest rate only if annual interest rate is valid for chequing account
        /// </summary>
        public override float AnnualIntrRate
        {
            //check to ensure the annual interest rate is valid for a checquing account
            set
            {
                if(value > MAX_INTEREST_RATE)
                {
                    Console.WriteLine("A checquing account cannot have an interest rate greater than " + MAX_INTEREST_RATE);
                }
                else
                {
                    _annualIntrRate = value / 100f;
                }
            }
            //use the base class to set the annual interest rate

        }

        /// <summary>
        ///Withdraw the given amount from the account and return the new balance.
        /// </summary>
        public override double withdraw(float amount)
        {
            //check the amount provided to ensure it is valid
            if (amount < 0)
            {
                Console.WriteLine("\nCannot withdraw negative amount, Please try again");
                return 0;
            }

            //check the overdraft on top of the actual balance
            if(amount > _balance + OVERDRAFT_LIMIT)
            {
                Console.WriteLine("Insufficant funds, cannot withdraw given amount");
                return 0;
            }

            //change the balance
            _balance -= amount;

            //provide the new balance to the caller to avoid a getBalance() call
            return _balance;
        }
    }
}
