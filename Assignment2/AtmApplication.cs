﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2
{
    /// <summary>
    /// Atm class that creates the ATM and bank objects and starts the ATM
    /// </summary>
    class AtmApplication
    {
        /// <summary>
        /// Main method that starts the ATM
        /// </summary>
        static void Main()
        {
            AtmApplication app = new AtmApplication();
            app.Run();
        }

        /// <summary>
        /// main method of the application object. Invoked as soon as an
        /// application object is created by Main(). Will create a bank object and start an ATM
        /// that operates in the context of the bank
        /// </summary>
        private void Run()
        {
            //create a bank for a more real-life like implementation
            Bank theBank = new Bank();

            //create ATM and link it with the bank
            Atm atm = new Atm(theBank);

            //start the ATM machine
            atm.start();

        }
    }
}
